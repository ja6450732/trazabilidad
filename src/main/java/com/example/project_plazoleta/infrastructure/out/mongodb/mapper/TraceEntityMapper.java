package com.example.project_plazoleta.infrastructure.out.mongodb.mapper;

import com.example.project_plazoleta.domain.model.Trace;
import com.example.project_plazoleta.infrastructure.out.mongodb.entity.TraceEntity;

import java.util.List;

public interface TraceEntityMapper {

    TraceEntity toEntity(Trace trace);

    Trace toTrace(TraceEntity trace);

    List<Trace> toTraceList(List<Trace> traceList);


}
