package com.example.project_plazoleta.infrastructure.out.mongodb.repository;

import com.example.project_plazoleta.infrastructure.out.mongodb.entity.TraceEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TraceRepository extends MongoRepository<TraceEntity, String> {
}
