package com.example.project_plazoleta.infrastructure.out.mongodb.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@Document("Trace")
public class TraceEntity {

    @Id
    private String idTrace;

    private Long orderId;

    private Long idClient;

    private String userEmail;

    private LocalDateTime date;

    private String stateBefore;

    private String stateCurrent;

    private Long idWorker;

    private String workerEmail;


}
