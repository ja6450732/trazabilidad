package com.example.project_plazoleta.infrastructure.out.mongodb.adapter;

import com.example.project_plazoleta.domain.model.Trace;
import com.example.project_plazoleta.domain.model.TraceTime;
import com.example.project_plazoleta.domain.model.WorkerRanking;
import com.example.project_plazoleta.domain.spi.TracePersistencePort;

import java.util.List;

public class TraceMongoAdapter implements TracePersistencePort {
    @Override
    public void saveTraceability(Trace traceability) {

    }

    @Override
    public List<Trace> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        return null;
    }

    @Override
    public List<TraceTime> getTraceabilityTimesOrders(String token, Long restaurantId) {
        return null;
    }

    @Override
    public List<WorkerRanking> getRankingEmployees(String token, Long restaurantId) {
        return null;
    }
}
