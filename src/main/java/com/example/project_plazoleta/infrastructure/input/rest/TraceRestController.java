package com.example.project_plazoleta.infrastructure.input.rest;


import com.example.project_plazoleta.application.dto.response.TraceResponseDto;
import com.example.project_plazoleta.application.dto.response.TraceTimeResponseDto;
import com.example.project_plazoleta.application.dto.response.WorkerRankingResponseDto;
import com.example.project_plazoleta.application.handler.impl.TraceHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trace")
@RequiredArgsConstructor
public class TraceRestController {

    private final TraceHandler traceHandler;

    @PostMapping("/create-log")
    public ResponseEntity<Void> saveTraceability() {

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/logs/{orderId}")
    public ResponseEntity<List<TraceResponseDto>> getTraceabilityByOrderAndCustomer(@RequestHeader("Authorization") String token, @PathVariable Long orderId) {
        return new ResponseEntity<>(traceHandler.getTraceabilityByOrderAndCustomer(token, orderId), HttpStatus.OK);
    }

    @GetMapping("/times/{restaurantId}")
    public ResponseEntity<List<TraceTimeResponseDto>> getTraceabilityTimesOrders(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId) {
        return null;
    }

    @GetMapping("/ranking/{restaurantId}")
    public ResponseEntity<List<WorkerRankingResponseDto>> getRankingEmployees(@RequestHeader("Authorization") String token, @PathVariable Long restaurantId) {
        return new ResponseEntity<>(traceHandler.getRankingEmployees(token, restaurantId), HttpStatus.OK);
    }


}
