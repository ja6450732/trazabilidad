package com.example.project_plazoleta.application.mapper;


import com.example.project_plazoleta.application.dto.response.TraceResponseDto;
import com.example.project_plazoleta.domain.model.Trace;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface TraceResponseMapper {

    TraceResponseDto toResponse(Trace trace);

    List<TraceResponseDto> toResponseList(List<Trace> traceList);


}
