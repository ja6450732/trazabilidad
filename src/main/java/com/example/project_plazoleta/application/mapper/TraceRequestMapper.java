package com.example.project_plazoleta.application.mapper;

import com.example.project_plazoleta.application.dto.request.TraceRequestDto;
import com.example.project_plazoleta.domain.model.Trace;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface TraceRequestMapper {

    Trace toTrace(TraceRequestDto traceRequestDto);

}
