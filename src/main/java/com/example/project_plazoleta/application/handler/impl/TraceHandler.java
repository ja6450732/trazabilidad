package com.example.project_plazoleta.application.handler.impl;

import com.example.project_plazoleta.application.dto.request.TraceRequestDto;
import com.example.project_plazoleta.application.dto.response.TraceResponseDto;
import com.example.project_plazoleta.application.dto.response.WorkerRankingResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class TraceHandler implements TraceHandlerInterface{

    @Override
    public void saveTraceability(TraceRequestDto traceabilityRequest) {

    }

    @Override
    public List<TraceResponseDto> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        return null;
    }

    @Override
    public List<TraceResponseDto> getTraceabilityTimesOrders(String token, Long restaurantId) {
        return null;
    }

    @Override
    public List<WorkerRankingResponseDto> getRankingEmployees(String token, Long restaurantId) {
        return null;
    }
}
