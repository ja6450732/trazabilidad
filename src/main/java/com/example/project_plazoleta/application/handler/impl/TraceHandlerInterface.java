package com.example.project_plazoleta.application.handler.impl;

import com.example.project_plazoleta.application.dto.request.TraceRequestDto;
import com.example.project_plazoleta.application.dto.response.TraceResponseDto;
import com.example.project_plazoleta.application.dto.response.WorkerRankingResponseDto;

import java.util.List;

public interface TraceHandlerInterface {

    void saveTraceability(TraceRequestDto traceabilityRequest);

    List<TraceResponseDto> getTraceabilityByOrderAndCustomer(String token, Long orderId);

    List<TraceResponseDto> getTraceabilityTimesOrders(String token, Long restaurantId);

    List<WorkerRankingResponseDto> getRankingEmployees(String token, Long restaurantId);


}
