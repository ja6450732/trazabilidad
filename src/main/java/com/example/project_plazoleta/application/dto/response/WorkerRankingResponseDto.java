package com.example.project_plazoleta.application.dto.response;

import lombok.Data;

@Data
public class WorkerRankingResponseDto {

    private Long employeeId;
    
    private Long averageTime;


}
