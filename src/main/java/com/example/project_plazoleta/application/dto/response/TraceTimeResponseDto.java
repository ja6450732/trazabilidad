package com.example.project_plazoleta.application.dto.response;


import lombok.Data;

@Data
public class TraceTimeResponseDto {

    private Long orderId;

    private Long time;

}
