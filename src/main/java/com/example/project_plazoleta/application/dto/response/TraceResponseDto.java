package com.example.project_plazoleta.application.dto.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TraceResponseDto {

    private String id;

    private Long orderId;

    private Long customerId;

    private String emailCustomer;

    private LocalDateTime date;

    private String stateBefore;

    private String stateCurrent;

    private Long employeeId;

    private String emailEmployee;

}
