package com.example.project_plazoleta.application.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum OrderStateEnum {

    CANCELLED,

    PENDING,

    UPCOMING,

    READY,

    DELIVERED

}
