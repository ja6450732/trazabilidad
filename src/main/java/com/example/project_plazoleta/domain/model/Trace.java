package com.example.project_plazoleta.domain.model;

import java.time.LocalDateTime;

public class Trace {

    private String id;
    private Long orderId;
    private Long customerId;
    private String emailCustomer;
    private LocalDateTime date;
    private String stateBefore;
    private String stateCurrent;
    private Long employeeId;
    private String emailEmployee;

    public Trace() {
    }

    public Trace(String id, Long orderId, Long customerId, String emailCustomer, LocalDateTime date, String stateBefore, String stateCurrent, Long employeeId, String emailEmployee) {
        this.id = id;
        this.orderId = orderId;
        this.customerId = customerId;
        this.emailCustomer = emailCustomer;
        this.date = date;
        this.stateBefore = stateBefore;
        this.stateCurrent = stateCurrent;
        this.employeeId = employeeId;
        this.emailEmployee = emailEmployee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getStateBefore() {
        return stateBefore;
    }

    public void setStateBefore(String stateBefore) {
        this.stateBefore = stateBefore;
    }

    public String getStateCurrent() {
        return stateCurrent;
    }

    public void setStateCurrent(String stateCurrent) {
        this.stateCurrent = stateCurrent;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmailEmployee() {
        return emailEmployee;
    }

    public void setEmailEmployee(String emailEmployee) {
        this.emailEmployee = emailEmployee;
    }


}
