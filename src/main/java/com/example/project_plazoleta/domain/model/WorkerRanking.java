package com.example.project_plazoleta.domain.model;

public class WorkerRanking {

    private Long employeeId;
    private Long averageTime;

    public WorkerRanking() {
    }

    public WorkerRanking(Long employeeId, Long averageTime) {
        this.employeeId = employeeId;
        this.averageTime = averageTime;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getAverageTime() {
        return averageTime;
    }

    public void setAverageTime(Long averageTime) {
        this.averageTime = averageTime;
    }

}
