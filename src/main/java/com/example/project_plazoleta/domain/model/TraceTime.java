package com.example.project_plazoleta.domain.model;

public class TraceTime {

    private Long orderId;
    private Long time;

    public TraceTime(Long orderId, Long time) {
        this.orderId = orderId;
        this.time = time;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

}
