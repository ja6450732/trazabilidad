package com.example.project_plazoleta.domain.usecase;

import com.example.project_plazoleta.domain.api.TraceServicePort;
import com.example.project_plazoleta.domain.model.Trace;
import com.example.project_plazoleta.domain.model.TraceTime;
import com.example.project_plazoleta.domain.model.WorkerRanking;

import java.util.List;

public class TraceUseCase implements TraceServicePort {


    @Override
    public void saveTraceability(Trace traceability) {

    }

    @Override
    public List<Trace> getTraceabilityByOrderAndCustomer(String token, Long orderId) {
        return null;
    }

    @Override
    public List<TraceTime> getTraceabilityTimesOrders(String token, Long restaurantId) {
        return null;
    }

    @Override
    public List<WorkerRanking> getRankingEmployees(String token, Long restaurantId) {
        return null;
    }
}
