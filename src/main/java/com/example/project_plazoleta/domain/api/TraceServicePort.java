package com.example.project_plazoleta.domain.api;

import com.example.project_plazoleta.domain.model.Trace;
import com.example.project_plazoleta.domain.model.TraceTime;
import com.example.project_plazoleta.domain.model.WorkerRanking;

import java.util.List;

public interface TraceServicePort {

    void saveTraceability(Trace traceability);

    List<Trace> getTraceabilityByOrderAndCustomer(String token, Long orderId);

    List<TraceTime> getTraceabilityTimesOrders(String token, Long restaurantId);

    List<WorkerRanking> getRankingEmployees(String token, Long restaurantId);


}
